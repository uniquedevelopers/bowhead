package bow.head.inc.feedback.Utils;

/**
 */

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;


/**
 */
public class Utils {

    public static final String TAG = Utils.class.getSimpleName();
    private static Utils utils = null;
    private static Context mAppContext;
    private static final AtomicInteger sNextGeneratedId = new AtomicInteger(1);
    private Dialog mTransparentDialog;

    public static String getFileSizeInMBorKB(File file) {
        if (file.exists()) {

            double bytes = file.length();
            double kilobytes = (bytes / 1024);
            double megabytes = (kilobytes / 1024);
            double gigabytes = (megabytes / 1024);
            double terabytes = (gigabytes / 1024);
            double petabytes = (terabytes / 1024);
            double exabytes = (petabytes / 1024);
            double zettabytes = (exabytes / 1024);
            double yottabytes = (zettabytes / 1024);

/*
            System.out.println("bytes : " + bytes);
            System.out.println("kilobytes : " + kilobytes);
            System.out.println("megabytes : " + megabytes);
            System.out.println("gigabytes : " + gigabytes);
            System.out.println("terabytes : " + terabytes);
            System.out.println("petabytes : " + petabytes);
            System.out.println("exabytes : " + exabytes);
            System.out.println("zettabytes : " + zettabytes);
            System.out.println("yottabytes : " + yottabytes);
*/

            System.out.println("megabytes wit 2 decimal only : " + String.format("%.2f", megabytes));
            if (megabytes > 1)
                return String.format("%.2f", megabytes) + " mb";
            else if (kilobytes > 1)
                return String.format("%.2f", kilobytes) + " kb";
            else
                return String.format("%.2f", bytes) + " b";
        } else {
            System.out.println("File does not exists!");
        }
        return "--";
    }

    public static String getAudioDuration(File file, Context context) {
        String formatted = "--:--";
        try {
            Uri uri = Uri.parse(file.getAbsolutePath());
            MediaMetadataRetriever mmr = new MediaMetadataRetriever();
            mmr.setDataSource(context, uri);
            String durationStr = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
            int millis = Integer.parseInt(durationStr);

            long hours = TimeUnit.MILLISECONDS.toHours(millis);
            long minutes;
            long seconds;

            formatted = String.format("%02d:%02d:%02d",
                    TimeUnit.MILLISECONDS.toHours(millis),
                    TimeUnit.MILLISECONDS.toMinutes(millis) -
                            TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)), // The change is in this line
                    TimeUnit.MILLISECONDS.toSeconds(millis) -
                            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
        } catch (Exception e) {
            e.printStackTrace();
            return "--:--";
        }
        return formatted;
    }

    public static String getAudioDuration(int millis) {
        String formatted = "--:--";
        try {

            long hours = TimeUnit.MILLISECONDS.toHours(millis);
            long minutes;
            long seconds;

            formatted = String.format("%02d:%02d:%02d",
                    TimeUnit.MILLISECONDS.toHours(millis),
                    TimeUnit.MILLISECONDS.toMinutes(millis) -
                            TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)), // The change is in this line
                    TimeUnit.MILLISECONDS.toSeconds(millis) -
                            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
        } catch (Exception e) {
            e.printStackTrace();
            return "--:--";
        }
        return formatted;
    }

    public static void shareIt(Activity activity, String appURL) {
        Intent sharingIntent = new Intent();
        sharingIntent.setAction(Intent.ACTION_SEND);
        sharingIntent.putExtra(Intent.EXTRA_TEXT, appURL);
        sharingIntent.setType("text/plain");
        activity.startActivity(Intent.createChooser(sharingIntent, "Share via"));
    }

    public static String replaceExt(String fileName) {
        return fileName.replace(".wav","").replace(".mp3","")
                .replace(".WAV","").replace(".MP3","");

    }

    public enum DialogType {
        ForceUpdate, TokenExpire, SessionExpired
    }


    // private static String TAG=AconUtil.class;

    public enum booleanEnum {TRUE, FALSE}

    public static Context getmAppContext() {
        return mAppContext;
    }

    private Utils(Context context) {
        mAppContext = context;
    }

    public static Utils getInstance(Context context) {
        if (utils == null)
            utils = new Utils(context);

        return utils;
    }

    public void showToast(String message, boolean showLong) {
        if (showLong) {
            Toast.makeText(mAppContext, message, Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(mAppContext, message, Toast.LENGTH_SHORT).show();
        }

    }

    public static int generateViewId() {
        for (; ; ) {
            final int result = sNextGeneratedId.get();
            // aapt-generated IDs have the high byte nonzero; clamp to the range under that.
            int newValue = result + 1;
            if (newValue > 0x00FFFFFF) newValue = 1; // Roll over to 1, not 0.
            if (sNextGeneratedId.compareAndSet(result, newValue)) {
                return result;
            }
        }
    }

    /*public void showForceDialog(final DialogType dialogType, final Context context) {
        dismissDialog();
        mTransparentDialog = null;
        mTransparentDialog = new Dialog(context, R.style.MaterialDialogSheetCareerPathWithPadding);
        mTransparentDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mTransparentDialog.setCancelable(false);

        mTransparentDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View view = inflater.inflate(R.layout.force_dialog, null);
        view.setPadding(0, 0, 0, 0);
        mTransparentDialog.setContentView(view);
        mTransparentDialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);


        TextView mTitle = view.findViewById(R.id.tv_title);
        TextView mMessage = view.findViewById(R.id.tv_message);
        TextView mOk = view.findViewById(R.id.bt_ok);

        if (dialogType == DialogType.ForceUpdate) {
            mTitle.setText(R.string.forceUpdate_title);
            mMessage.setText(R.string.force_update_message);

        } else if (dialogType == DialogType.SessionExpired) {
            mTitle.setText(R.string.session_expired_title);
            mMessage.setText(R.string.session_expired_message);

        } else if (dialogType == DialogType.TokenExpire) {
            mTitle.setText(R.string.session_expired_title);
            mMessage.setText(R.string.session_expired_message);
        }


        if (!((Activity) context).isFinishing())
            mTransparentDialog.show();
    }

    public static void goToLogIn(Context context) {
        if (context == null || !(context instanceof Activity)) {
            Logger.debugLog(TAG, "Invalid context!! .. will not redirect to hive app");
            return;
        }
        PreferencesWrapper.getInstance(context).clearAllPreferences();
        Activity activity = (Activity) context;
    }
*/
    private void dismissDialog() {
        try {
            if (mTransparentDialog != null && mTransparentDialog.isShowing()) {
                mTransparentDialog.dismiss();
            }
        } catch (Exception e) {

        }
    }

    public byte[] getBytes(InputStream is) throws IOException {

        int len;
        int size = 1024;
        byte[] buf;
        if (is instanceof ByteArrayInputStream) {
            size = is.available();
            buf = new byte[size];
            len = is.read(buf, 0, size);
        } else {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            buf = new byte[size];
            while ((len = is.read(buf, 0, size)) != -1)
                bos.write(buf, 0, len);
            buf = bos.toByteArray();
        }
        return buf;
    }


    public static void hideKeyBorad(Context context, View view) {
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) (context.getSystemService(Context.INPUT_METHOD_SERVICE));
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static boolean isNetworkAvailable(Context ctx) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static long getDateInMillis(String srcDate) {
        String format = "yyyy-MM-dd HH:mm:ss";//2018-05-16 15:17:08
        String contain = "+05:30";
        if (srcDate != null && srcDate.toLowerCase().contains(contain)) {
            format = "MM/dd/yyyy HH:mm:ss +05:30";
        }
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        sdf.setTimeZone(TimeZone.getDefault());
        Date d;
        try {
            long dateInMillis = 0;
            if (srcDate != null) {
                d = sdf.parse(srcDate);
                dateInMillis = d.getTime();
            }
            return dateInMillis;
        } catch (ParseException e) {
            Logger.debugLog("AconUtil", "Exception caught " + e.toString());
        }
        return 0;
    }

    public static long getDateInMillis1(String srcDate) {
        String format = "yyyy-MM-dd HH:mm:ss'Z'";
        String contain = "+05:30";
        if (srcDate != null && srcDate.toLowerCase().contains(contain)) {
            format = "yyyy-MM-dd HH:mm:ss+05:30";
        }
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        sdf.setTimeZone(TimeZone.getDefault());
        Date d;
        try {
            long dateInMillis = 0;
            if (srcDate != null) {
                d = sdf.parse(srcDate);
                dateInMillis = d.getTime();
            }
            return dateInMillis;
        } catch (ParseException e) {
            Logger.debugLog("AconUtil", "Exception caught " + e.toString());
        }
        return 0;
    }

    public static long getDateInMillisCP(String srcDate) {
        String format = "yyyy-MM-dd'T'HH:mm:ss";
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        sdf.setTimeZone(TimeZone.getDefault());
        Date d;
        try {
            long dateInMillis = 0;
            d = sdf.parse(srcDate);
            dateInMillis = d.getTime();
            return dateInMillis;
        } catch (ParseException e) {
            Logger.debugLog("AconUtil", "Exception caught " + e.toString());
        }
        return 0;
    }

    public static long getDateInMillisMediaLiveComments(String srcDate) {
        String format = "yyyy-MM-dd' 'HH:mm:ss+05:30";
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        sdf.setTimeZone(TimeZone.getDefault());
        Date d;
        try {
            long dateInMillis = 0;
            d = sdf.parse(srcDate);
            dateInMillis = d.getTime();
            return dateInMillis;
        } catch (ParseException e) {
            Logger.debugLog("AconUtil", "Exception caught " + e.toString());
        }
        return 0;
    }

    public static long getDateInMillisWithSpace(String srcDate) {


        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss+05:30");


        sdf.setTimeZone(TimeZone.getDefault());

        Date d;
        try {
            long dateInMillis = 0;
            d = sdf.parse(srcDate);


            dateInMillis = d.getTime();
            return dateInMillis;
        } catch (ParseException e) {

        }

        return 0;
    }

    public static long getCurrenttimeInMilliseconds() {
        long dateInMillis = 0;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String currentDateandTime = sdf.format(new Date());
        try {
            dateInMillis = sdf.parse(currentDateandTime).getTime();
        } catch (ParseException e) {
            Logger.errorLog("AconUtil", e.getMessage());

        }
        return dateInMillis;
    }


    public static boolean isConnectedToInternet(Context context, boolean showToast) {
        if (context == null) {
            return false;
        }
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo info = cm.getActiveNetworkInfo();

        boolean isConnected = (info != null && info.isConnected());
        if (!isConnected && showToast) {
            getInstance(context).showToast("Please check your internet connection", false);
        }
        return isConnected;
    }

    public static String changeToDateFormat(long time, int type) {
        SimpleDateFormat dateFormat;
        if (type == 1) {
            //dateFormat = new SimpleDateFormat("dd MMM yy hh:mm aa");
            dateFormat = new SimpleDateFormat("yyyyMMddhhmmss", Locale.getDefault());
        } else if (type == 2) {
            dateFormat = new SimpleDateFormat("dd MMM yy", Locale.getDefault());
        } else if (type == 3) {
            dateFormat = new SimpleDateFormat("sss", Locale.getDefault());
        } else if (type == 4) {
            dateFormat = new SimpleDateFormat("dd MMM HH:mm", Locale.getDefault());
//        } else if (type == 4) {
//            dateFormat = new SimpleDateFormat("dd MMM | HH:mm");
        } else if (type == 5) {
            dateFormat = new SimpleDateFormat("hhmm", Locale.getDefault());
        } else if (type == 6) {
            dateFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
        } else if (type == 7) {
            dateFormat = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());
        } else if (type == 8) {
            dateFormat = new SimpleDateFormat("yyyy", Locale.getDefault());
        } else if (type == 9) {
            dateFormat = new SimpleDateFormat("MM", Locale.getDefault());
        } else if (type == 10) {
            String relativeTime = String.valueOf(DateUtils.getRelativeTimeSpanString(time, System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS));
            return relativeTime;
        } else if (type == 11) {
            dateFormat = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());
        } else if (type == 12) {
            dateFormat = new SimpleDateFormat("dd MMM, yyyy", Locale.getDefault());
        } else if (type == 13) {
            dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");//2017-10-26T07:32:21Z
            dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        } else {
            dateFormat = new SimpleDateFormat();
        }
        return dateFormat.format(new Date(time));
    }

    //    public static boolean appInstalledOrNot(String uri) {
//        PackageManager pm = getContext().getPackageManager();
//        try {
//            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
//            return true;
//        } catch (PackageManager.NameNotFoundException e) {
//        }
//
//        return false;
//    }
    public static String getMonth(int month) {
        return new DateFormatSymbols().getMonths()[month];
    }


    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public static float round(float value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (float) tmp / factor;
    }

    public static int dpToPx(int dp, DisplayMetrics displayMetrics) {
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    public static int dpToPxfloat(float dp, DisplayMetrics displayMetrics) {
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }


    public static int pxToDp(int px, DisplayMetrics displayMetrics) {
        return Math.round(px / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    public static void hideKeyboardWithContext(Context context) {
        try {
            InputMethodManager imm = (InputMethodManager) (context.getSystemService(Context.INPUT_METHOD_SERVICE));
            View view = ((Activity) context).getWindow().getDecorView().findViewById(android.R.id.content);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (Exception e) {
//            just in case
        }
    }

    public static String handleRetrofitExceptions(Throwable t) {
        String result = null;
        if (t instanceof java.net.ConnectException) {
            result = "Unable to connect. Please try again";
        } else if (t instanceof java.net.SocketTimeoutException) {
            result = "Request timed out. Please try again";
        } else if (t instanceof java.net.UnknownHostException) {
            result = "Please check your internet connection";
        }
        if (result != null && !result.isEmpty()) {
            Utils.getInstance(null).showToast(result, false);
        } else {
            Utils.getInstance(null).showToast(Constants.ERROR_MESSAGE, false);
        }
        return result;
    }

    public static String getDeviceId(ContentResolver resolver) {
        return Settings.Secure.getString(resolver, Settings.Secure.ANDROID_ID);
    }

    public static boolean responseValid(String json, Context mContext) {
        boolean result = false;
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(json);
            if (jsonObject.has("status")) {
                jsonObject = jsonObject.getJSONObject("status");
            } else {
                if (jsonObject.has("status")) {
                    return true;
                }

            }


        } catch (JSONException e) {
            Utils.getInstance(null).showToast("Something went wrong. Please try again.", false);
        } catch (OutOfMemoryError error) {
            if (mContext != null)
                Utils.getInstance(null).showToast("mContext.getString(R.string.out_of_memory)", true);
        }
        return result;
    }

    public final static boolean isValidMail(CharSequence target) {
        if (TextUtils.isEmpty(target)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public static String validateAnyString(String string) {
        if (string == null) {
            return "";
        } else {
            return string;
        }
    }

    public static String validateAnyStringAsInt(String string) {
        if (string == null || string.isEmpty() || string.length() == 0 || string.trim().equalsIgnoreCase("")) {
            return "0";
        } else {
            return string;
        }
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static void setImage(final String s, final ImageView iv, Context mContext) {

        byte[] bytes = Base64.decode(s, Base64.NO_WRAP);

        Logger.errorLog("LENGTH", bytes.length + "");

        BitmapFactory.Options options = new BitmapFactory.Options();
        final Bitmap decodedByte = BitmapFactory.decodeByteArray(bytes, 0, bytes.length, options);

        ((Activity) mContext).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    iv.setImageBitmap(decodedByte);
                } catch (Exception e) {
                } catch (OutOfMemoryError e) {

                }
            }
        });

    }


    private boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }


    public static String getExternalSdCardPath() {
        String path = null;

        File sdCardFile = null;
        List<String> sdCardPossiblePath = Arrays.asList("external_sd", "ext_sd", "external", "extSdCard");

        for (String sdPath : sdCardPossiblePath) {
            File file = new File("/mnt/", sdPath);

            if (file.isDirectory() && file.canWrite()) {
                path = file.getAbsolutePath();

                String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmmss").format(new Date());
                File testWritable = new File(path, "test_" + timeStamp);

                if (testWritable.mkdirs()) {
                    testWritable.delete();
                } else {
                    path = null;
                }
            }
        }

        if (path != null) {
            sdCardFile = new File(path);
        } else {
            sdCardFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath());
        }

        return sdCardFile.getAbsolutePath();
    }

    public static String getExternalStorageDirectoryPathForGigl() {
        File directory = new File(getExternalSdCardPath()
                + "/" + Constants.STORAGE_DIRECTORY);

        if (!directory.exists()) {
            directory.mkdirs();
        }
        Logger.debugLog("PATH", "PATH" + directory.getAbsolutePath() + "/");
        return directory.getAbsolutePath() + "/";
    }

}
