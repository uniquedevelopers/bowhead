package bow.head.inc.feedback.network;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import bow.head.inc.feedback.Utils.Constants;
import bow.head.inc.feedback.Utils.Logger;
import bow.head.inc.feedback.Utils.Utils;
import bow.head.inc.feedback.base.BaseApplication;
import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestClient {
    private static API REST_CLIENT;

    static {
        setupRestClient();
    }

    private RestClient() {
    }

    public static API get() {
        return REST_CLIENT;
    }

    public static void setupRestClient() {

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder()
                .cache(new Cache(BaseApplication.getCacheDirM(), 10 * 1024 * 1024))
                .connectTimeout(120, TimeUnit.SECONDS)
                .readTimeout(120, TimeUnit.SECONDS);
//                .writeTimeout(120, TimeUnit.SECONDS);
        httpClient.interceptors().add(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {

                //For Response cache
                Request request = chain.request();
                if (Utils.isNetworkAvailable(Utils.getmAppContext())) {
                    request = request.newBuilder().header("Cache-Control", "public, max-age=" + 60).build();
                } else {
                    request = request.newBuilder().header("Cache-Control", "public, only-if-cached, max-stale=" + 60 * 60 * 24 * 7).build();
                }

/*//                To add headers
                Request request = chain.request();*/
                request = request.newBuilder()
                        .header("Content-Type", "application/json")
                        .build();
                Logger.debugLog(getClass().getSimpleName(), "request.body : " + request.toString());

                return chain.proceed(request);//Crash here
            }
        });

        //Logs body only in debug mode
        if (Constants.DEBUG) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            httpClient.addInterceptor(logging);
        }


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();
        REST_CLIENT = retrofit.create(API.class);
    }
}
