package bow.head.inc.feedback.Utils;

import android.content.Context;
import android.util.Log;

import bow.head.inc.feedback.BuildConfig;


/**
 */


public class Logger {

    private static boolean debug = BuildConfig.DEBUG;

    public static void debugLog(String tag, String msg) {
        if (debug) {
            Log.d(tag, msg);
        }
    }

    public static void verboseLog(String tag, String msg) {
        if (debug) {
            Log.v(tag, msg);
        }
    }

    public static void errorLog(String tag, String msg) {
        if (debug) {
            Log.e(tag, msg);
        }
    }

    public static void warnLog(Context ctx, String tag, String msg) {
        if (debug) {
            Log.w(tag, msg);
        }
    }
}
