package bow.head.inc.feedback;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.gson.Gson;

import bow.head.inc.feedback.Utils.Logger;
import bow.head.inc.feedback.bean.UserLogin;
import bow.head.inc.feedback.network.NetworkController;
import bow.head.inc.feedback.network.ResponseCallback;
import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;

public class SignInActivity extends AppCompatActivity {


    private static final String TAG = SignInActivity.class.getSimpleName();
    @BindView(R.id.client_id)
    EditText clientId;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.login)
    AppCompatButton login;

    @BindView(R.id.progress_layout)
    RelativeLayout mProgressLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        ButterKnife.bind(this);

    }

    public void signIn(View view) {
        if (clientId.getText().toString().trim().equals("")) {
            Toast.makeText(SignInActivity.this, "Please Enter Client ID.", Toast.LENGTH_SHORT).show();
        } else if (password.getText().toString().trim().equals("")) {
            Toast.makeText(SignInActivity.this, "Please Enter Password.", Toast.LENGTH_SHORT).show();
        } else {
            userLogin(clientId.getText().toString().trim(), password.getText().toString().trim());
        }
    }

    private void dismissProgressBar() {
        try {
            mProgressLayout.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showProgressBar() {
        mProgressLayout.setVisibility(View.VISIBLE);
    }

    private void userLogin(String clientId, String password) {
        showProgressBar();

        NetworkController.with(this).userLogin(clientId, password, new ResponseCallback<Object>(this) {

            @Override
            protected void onResponse(Object data) {
                dismissProgressBar();
                try {
                    String json = new Gson().toJson(data);

                    UserLogin login = new Gson().fromJson(json, UserLogin.class);
                    Logger.debugLog(TAG, TAG + json);

                    if (login.getError() == null) {
                        if (login.getResponse().getRespCode().contains("S200")) {
                            moveToFeedback();
                        }
                    } else {
                        Toast.makeText(SignInActivity.this, login.getError().getMessage(), Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(SignInActivity.this, "Error while login.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            protected void onFailure(Throwable t) {
                dismissProgressBar();
                Toast.makeText(SignInActivity.this, "Some error occurred", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void moveToFeedback() {
        Intent intent = new Intent(this, FeedbackActivity.class);
        startActivity(intent);
        finish();
    }


    public void changePassword(View view) {
    }
}
