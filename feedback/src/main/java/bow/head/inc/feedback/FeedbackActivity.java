package bow.head.inc.feedback;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.gson.Gson;

import bow.head.inc.feedback.Utils.Logger;
import bow.head.inc.feedback.Utils.MessageDialog;
import bow.head.inc.feedback.Utils.Utils;
import bow.head.inc.feedback.bean.UserLogin;
import bow.head.inc.feedback.network.NetworkController;
import bow.head.inc.feedback.network.ResponseCallback;
import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;

public class FeedbackActivity extends AppCompatActivity {

    private static final String TAG = FeedbackActivity.class.getSimpleName();
    @BindView(R.id.name)
    EditText name;
    @BindView(R.id.phone)
    EditText phone;
    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.comment)
    EditText comment;

    @BindView(R.id.progress_layout)
    RelativeLayout mProgressLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        ButterKnife.bind(this);
    }

    public void showThankYouDialog() {
        name.setText("");
        phone.setText("");
        email.setText("");
        comment.setText("");
        name.requestFocus();

        MessageDialog messagedialog=new MessageDialog(this);
        messagedialog.showMessageDialog("Thank you for your valuable feedback.","Success");
    }

    public void submitFeedback(View view) {
        if (name.getText().toString().trim().equals("")) {
            Toast.makeText(FeedbackActivity.this, "Please Enter Name.", Toast.LENGTH_SHORT).show();
        } else if (phone.getText().toString().trim().equals("")) {
            Toast.makeText(FeedbackActivity.this, "Please Enter Phone.", Toast.LENGTH_SHORT).show();
        } else if (email.getText().toString().trim().equals("")) {
            Toast.makeText(FeedbackActivity.this, "Please Enter Email.", Toast.LENGTH_SHORT).show();
        } else if(!Utils.isValidMail(email.getText().toString().trim())){
            Toast.makeText(FeedbackActivity.this, "Please Enter a valid Email.", Toast.LENGTH_SHORT).show();
        } else if (comment.getText().toString().trim().equals("")) {
            Toast.makeText(FeedbackActivity.this, "Please Enter Comment.", Toast.LENGTH_SHORT).show();
        } else {
            userLogin(name.getText().toString().trim(), phone.getText().toString().trim(),
                    email.getText().toString().trim(),comment.getText().toString().trim());
        }
    }

    private void userLogin(String name, String phone, String email, String comment) {
        showProgressBar();

        NetworkController.with(this).submitFeedback(name, phone, email, comment, new ResponseCallback<Object>(this) {

            @Override
            protected void onResponse(Object data) {
                dismissProgressBar();
                try {
                    String json = new Gson().toJson(data);

                    UserLogin login = new Gson().fromJson(json, UserLogin.class);
                    Logger.debugLog(TAG, TAG + json);

                    if (login.getError() == null) {
                        if (login.getResponse().getRespCode().contains("S200")) {
                            showThankYouDialog();
                        }
                    } else {
                        Toast.makeText(FeedbackActivity.this, login.getError().getMessage(), Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
//                    Toast.makeText(FeedbackActivity.this, "Error while login.", Toast.LENGTH_SHORT).show();
                    showThankYouDialog();
                }
            }

            @Override
            protected void onFailure(Throwable t) {
                dismissProgressBar();
                Toast.makeText(FeedbackActivity.this, "Some error occurred", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void dismissProgressBar() {
        try {
            mProgressLayout.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showProgressBar() {
        mProgressLayout.setVisibility(View.VISIBLE);
    }

}
