package bow.head.inc.feedback.Utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import bow.head.inc.feedback.R;

public class MessageDialog {

    CardView card_warning_dialog;
    Context context;
    public MessageDialog(Context context){
        this.context=context;
    }

    AlertDialog d;
    public void showMessageDialog(String message,String type){
        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        builder1.setCancelable(true);

        LayoutInflater inflaterr = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewtemplelayout = inflaterr.inflate(R.layout.thank_you_dialog, null);

        RelativeLayout layout_warning = (RelativeLayout) viewtemplelayout.findViewById(R.id.layout_warning);
        layout_warning.setVisibility(View.VISIBLE);

        TextView tv_description = (TextView) viewtemplelayout.findViewById(R.id.tv_description);
        card_warning_dialog= (CardView) viewtemplelayout.findViewById(R.id.card_warning_dialog);
        ImageView img_type = (ImageView) viewtemplelayout.findViewById(R.id.img_type);
        AppCompatButton okay = (AppCompatButton) viewtemplelayout.findViewById(R.id.okay);
        okay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if(d!=null)
                        d.cancel();
                    /*if (context != null && context instanceof Activity)
                        ((Activity) context).finish();*/
                }catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        });
        tv_description.setText(message);

        if(type.equals("Warning")) {
            //Formatting view
            card_warning_dialog.setCardBackgroundColor(ContextCompat.getColor(context, R.color.warning));
            tv_description.setTextColor(ContextCompat.getColor(context, R.color.error));
        }
        if(type.equals("Success")) {
            //Formatting view
//            img_type.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.green_smiley));
//            card_warning_dialog.setCardBackgroundColor(Color.parseColor("#8BC34A"));
//            tv_description.setTextColor(ContextCompat.getColor(context, R.color.white));
        }
        d = builder1.setView(viewtemplelayout).create();
        d.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        //Dialog Animation.
        d.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        d.show();
    }


    // Checking for all possible internet providers
    public boolean isConnectedToInternet(){

        ConnectivityManager connectivity =
                (ConnectivityManager) context.getSystemService(
                        Context.CONNECTIVITY_SERVICE);
        if (connectivity != null)
        {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED)
                    {
                        return true;
                    }
        }
        return false;
    }

}