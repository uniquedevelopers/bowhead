package bow.head.inc.feedback.network;

import java.lang.ref.Reference;
import java.util.LinkedHashMap;

import bow.head.inc.feedback.Utils.Constants;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HeaderMap;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 */

public interface API {


    @POST(Constants.REGISTER)
    Call<ResponseCallback> getItem(@HeaderMap LinkedHashMap<String, String> headers, @Body LinkedHashMap<String, Object> params);

    @FormUrlEncoded
    @POST(Constants.REGISTER)
    Call<Object> doRegister(@Field("fname") String fname, @Field("lname") String lname, @Field("email") String email, @Field("phone") String phone, @Field("password") String password, @Field("device") String device, @Field("deviceid") String deviceId);

    @GET(Constants.USERLOGIN_NORMAL)
    Call<Object> userLogin(@Query("clientUsername") String clientUsername, @Query("clientPassword") String clientPassword);

    Reference doRegisterWithGoogle(String appid, String displayNAme, String lname, String email, String phone, String android, String deviceId);

    @FormUrlEncoded
    @POST(Constants.SUBMIT_FEEDBACK)
    Call<Object> submitFeedback(@Field("data") String dataJsonObject);

    @FormUrlEncoded
    @POST(Constants.USERLOGIN_NORMAL)
    Call<Object> doRegisterWithFacebook(@Field("appid") String appid, @Field("fname") String fname, @Field("lname") String lname, @Field("username") String email, @Field("phone") String phone, @Field("device") String device, @Field("deviceid") String deviceId, @Field("source") String source);

    //@FormUrlEncoded
    @POST(Constants.BOOKDOCK)
    Call<Object> getAudioBookList();

    @GET(Constants.RECOMMENDATION_LIST)
    Call<Object> getRecommendationList();

    @FormUrlEncoded
    @POST(Constants.UPDATE_USER_PHONE)
    Call<Object> doUpdateUserPhone(@Field("user_id") String u_id, @Field("phone") String phone, @Field("device") String device, @Field("deviceid") String deviceId);

    @FormUrlEncoded
    @POST(Constants.UPDATE_DEVICE_TOKEN)
    Call<Object> updateDeviceToken(@Field("device_token") String device_token, @Field("user_id") String user_id, @Field("device") String device, @Field("device_id") String deviceId);

    @FormUrlEncoded
    @POST(Constants.UPDATE_LISTEN_COUNT)
    Call<Object> updateListenCount(@Field("post_id") String post_id, @Field("user_id") String user_id, @Field("device") String device, @Field("device_id") String deviceId);

    @FormUrlEncoded
    @POST(Constants.UPDATE_DOWNLOAD_COUNT)
    Call<Object> updateDownloadCount(@Field("post_id") String post_id, @Field("user_id") String user_id, @Field("device") String device, @Field("device_id") String deviceId);

//    @FormUrlEncoded
//    @POST(Constants.SUBMIT_RATING)
//    Call<Object> submitRating(@Field("post_id") String post_id, @Field("user_id") String user_id, @Field("device") String device, @Field("device_id") String deviceId, @Field("rating") String rating);

    @FormUrlEncoded
    @POST(Constants.CONTACT_US)
    Call<Object> submitContactUs(@Field("message") String message, @Field("phone") String phone, @Field("user_id") String user_id, @Field("device") String device, @Field("deviceId") String deviceId);

    @FormUrlEncoded
    @POST(Constants.ABOUT_US_API)
    Call<Object> getAboutUs(@Field("device") String device, @Field("deviceId") String deviceId);

    @FormUrlEncoded
    @POST(Constants.FAQ_API)
    Call<Object> getFaq(@Field("device") String device, @Field("deviceId") String deviceId);

    @FormUrlEncoded
    @POST(Constants.GET_POST_FAV_COMMENTS)
    Call<Object> getPostFavComments(@Field("postid") String postid, @Field("device") String device, @Field("deviceId") String deviceId);

    @FormUrlEncoded
    @POST(Constants.GET_POST_COMMENTS)
    Call<Object> getPostComments(@Field("postid") String postid, @Field("offset") String offset, @Field("device") String device, @Field("deviceId") String deviceId);

    @FormUrlEncoded
    @POST(Constants.ADD_COMMENT)
    Call<Object> addComment(@Field("comment") String comment, @Field("postid") String postid, @Field("userid") String userid, @Field("devicetoken") String deviceToken, @Field("device") String device, @Field("deviceid") String deviceId);

    //    @POST(Constants.BASE_URL)
//    Call<ResponseCallback> getItem(@HeaderMap LinkedHashMap<String, String> headers, @Body LinkedHashMap<String, Object> params);
//
//    @GET(Constants.BASE_URL+ "{paramName}")
//    Call<ResponseCallback> getLists(@HeaderMap LinkedHashMap<String, String> headers, @Path("paramName") String CategoryInternalName);

}
