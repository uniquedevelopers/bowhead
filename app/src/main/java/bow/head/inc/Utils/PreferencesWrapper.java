package bow.head.inc.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

/**
 */

public class PreferencesWrapper {
    private String TAG = getClass().getSimpleName();
    private SharedPreferences mPreferences;
    private static PreferencesWrapper mPreferencesWrapper;

    public static PreferencesWrapper getInstance(Context context) {
        if (mPreferencesWrapper == null) {
            mPreferencesWrapper = new PreferencesWrapper(context);
        }

        return mPreferencesWrapper;
    }

    private PreferencesWrapper(Context context) {
        mPreferences = context.getSharedPreferences(Constants.PREFERENCES, Context.MODE_PRIVATE);
    }


    public void putString(String key, String value) {

        mPreferences.edit().putString(key, value).apply();
    }

    public void putBoolean(String key, boolean value) {

        mPreferences.edit().putBoolean(key, value).apply();
    }

    public String getString(String key) {
        String decryptedValue = mPreferences.getString(key, null);
        if (TextUtils.isEmpty(decryptedValue)) {
            return null;
        }
        return decryptedValue;
    }

    public boolean checkExistence(String key) {
        String decryptedValue = mPreferences.getString(key, null);
        if(decryptedValue == null)
            return false;

        try {
            if(decryptedValue!=null)
                return true;
        } catch (Exception e) {
            Logger.debugLog(TAG, "Exception caught :" + e.getMessage());
        }
        return false;
    }


    public void putStringWithoutEncryption(String key, String value){
        mPreferences.edit().putString(key, value).apply();
    }

    public String getStringWithoutEncryption(String key){
       return mPreferences.getString(key,null);
    }

    public Boolean getBoolean(String key) {

        boolean decryptValue = mPreferences.getBoolean(key, false);

        return decryptValue;
    }

    public void clearAllPreferences(){
        mPreferences.edit().clear().apply();
    }

}
