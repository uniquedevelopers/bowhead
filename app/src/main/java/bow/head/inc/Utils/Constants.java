package bow.head.inc.Utils;

import com.example.hitesh.bowheadinc.BuildConfig;

/**
 *
 */

public class Constants {

    public static final boolean DEBUG = BuildConfig.DEBUG;
    public static final String BASE_URL = BuildConfig.BASE_URL;
    public static final String PREFERENCES = "App_Preference";
    public static final String APP_VERSION = BuildConfig.APP_VERSION;
    public static final String APP_SHARING_URL = "https://play.google.com/store/apps/details?id=com.gigl.app";
    public static final String ADMOB_APP_ID = "ca-app-pub-5362270618704567~7017838379";//"ca-app-pub-1414740234089956~4290413662";

    //Used in xml--
    public static final String LIVE_AD_UNIT_ID_FOR_BANNER = "ca-app-pub-1414740234089956/8447127007";
    public static final String SAMPLE_AD_UNIT_ID_FOR_BANNER = "ca-app-pub-3940256099942544/6300978111";
    //--

    public static final String STORAGE_DIRECTORY = "GIGL_DATA.nomedia";


    //Cache key constants And API's URL

    //Notification Api's

    //Response code constants
    public static final String RESPONSE_CODE_SUCCESS = "200";


    public static final String ERROR_MESSAGE = "Something went wrong. Please try again later";
    public static final String REGISTER = "register.php?";
    public static final String USERLOGIN_NORMAL = "login.php?";
    public static final String PREF_USER_ID = "u_id";
    public static final String PREF_USER_NAME = "username";
    public static final String PREF_FIRST_NAME = "fname";
    public static final String PREF_LAST_NAME = "lname";
    public static final String PREF_USER_PHONE = "phone";
    public static final String PREF_USER_LOGGED_IN = "logged_in";
    public static final String PREF_FCM_TOKEN = "fcm_token";
    public static final String BOOKDOCK = "bookdoc.php";
    public static final String RECOMMENDATION_LIST = "recommendoc.php";

    public static final String UPDATE_USER_PHONE = "update_regphone.php?";
    public static final String UPDATE_DEVICE_TOKEN = "update_devicetoken.php?";
    public static final String UPDATE_LISTEN_COUNT = "listen_count_api.php?";
    public static final String UPDATE_DOWNLOAD_COUNT = "download_count_api.php?";
    public static final String SUBMIT_RATING = "submit_rating_api.php?";
    public static final String CONTACT_US = "contactusapi.php?";
    public static final String ABOUT_US_API = "aboutusapi.php?";
    public static final String FAQ_API = "faqapi.php?";

    public static final String GET_POST_FAV_COMMENTS = "displaypostcomments.php?";
    public static final String GET_POST_COMMENTS = "displayallpostcomments.php?";
    public static final String ADD_COMMENT = "add_postcomments.php?";

    public static final String STATUS = "DOWNLOADED";
    public static final String FORGOT_URL = "http://App.greatideasgreatlife.com/forgotpassword";
    public static final String SOCIAL_FACEBOOK = "https://www.facebook.com/GreatIdeasGreatLife/";
    public static final String SOCIAL_YOUTUBE = "https://www.youtube.com/channel/UCurYxozQbvKn-oNUxNJh45Q";
    public static final String SOCIAL_INSTAGRAM = "https://www.instagram.com/motivation_greatideasgreatlife/";
    public static final String PREF_CURRENT_SCREEN = "current_screen";

    public enum FragmentType {
        Garbage(1008), MenuFragment(1001), Highlights(1002), AboutUs(1003), ProductFamily(1004), Events(1005),
        Awards(1006), ThoughtLeadership(1007), Product(1008);

        private final int value;

        private FragmentType(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

}
