package bow.head.inc.base;

import android.app.Application;
import android.content.Context;
import android.graphics.Bitmap;

import bow.head.inc.Utils.Utils;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

import java.io.File;

public class BaseApplication extends Application {
    private static final int PICASSO_DISK_CACHE_SIZE = 1024 * 1024 * 100;
    private static File f;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Utils.getInstance(getApplicationContext());
        setUpPicasso(getApplicationContext());
        f = getCacheDir();
    }

    public static File getCacheDirM() {
        return f;
    }


    public static void setUpPicasso(Context context) {

        Picasso.Builder picassoBuilder = new Picasso.Builder(context);
        picassoBuilder.downloader(new OkHttpDownloader(context, Integer.MAX_VALUE));
        picassoBuilder.defaultBitmapConfig(Bitmap.Config.ARGB_8888);
//        picassoBuilder.memoryCache(new LruCache(PICASSO_DISK_CACHE_SIZE));
//        picassoBuilder.indicatorsEnabled(true);
//        picassoBuilder.loggingEnabled(true);
        Picasso picasso = picassoBuilder.build();

        try {
            Picasso.setSingletonInstance(picasso);
        } catch (IllegalStateException ignored) {
            // Picasso instance was already set
            // cannot set it after Picasso.with(Context) was already in use
        }
    }
}
