package bow.head.inc;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Debug;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.menu.ActionMenuItemView;
import android.support.v7.widget.ActionMenuView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import bow.head.inc.Utils.Constants;
import bow.head.inc.Utils.Logger;
import bow.head.inc.Utils.PreferencesWrapper;
import bow.head.inc.Utils.Utils;
import bow.head.inc.network.NetworkController;
import bow.head.inc.network.ResponseCallback;

import com.example.hitesh.bowheadinc.BuildConfig;
import com.example.hitesh.bowheadinc.R;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.Signature;
import java.util.Timer;
import java.util.regex.Pattern;

public class SignInActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = SignInActivity.class.getSimpleName();
    GoogleSignInClient mGoogleSignInClient;
    int RC_SIGN_IN = 1;
    Button continueLogin;
    EditText userEmail;
    EditText createPassword;
    AccessToken accessTokenFb;
    RelativeLayout mProgressLayout;
    Button fb;
    CallbackManager callbackManager;


    ViewPager viewPager = null;
    int numberOfViewPagerChildren = 5;
    int lastIndexOfViewPagerChildren = numberOfViewPagerChildren - 1;

    Button signUp;
    Button signIn;
    LinearLayout loginLinLay;
    LinearLayout registerLinLay;

    int currentPage = 0;
    Timer timer;
    final long DELAY_MS = 000;//delay in milliseconds before task is to be executed
    final long PERIOD_MS = 3000;

    //__Register______
    private EditText fName;
    private EditText email;
    private EditText phone;
    private EditText password;
    private EditText c_password;
    private Button register;
    private static String deviceId;
    private View topLayout;
    private View loginLogo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        callbackManager = CallbackManager.Factory.create();
        setContentView(R.layout.activity_sign_in);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(ContextCompat.getColor(SignInActivity.this, R.color.white));
        }

        PackageInfo info;
        try {
            info = getPackageManager().getPackageInfo("bow.head.inc", PackageManager.GET_SIGNATURES);
            for (android.content.pm.Signature signature : info.signatures) {
                MessageDigest md;
                md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String something = new String(Base64.encode(md.digest(), 0));
                //String something = new String(Base64.encodeBytes(md.digest()));
                Log.e("hash key", something);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("no such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("exception", e.toString());
        }

//        colorizeToolbar(get, getResources().getColor(R.color.tab4), SignInActivity.this);

        deviceId = Utils.getDeviceId(getContentResolver());

        intiView();
        register.setOnClickListener(this);

        signUp = (Button) findViewById(R.id.signUp);
        signIn = (Button) findViewById(R.id.signIn);
        topLayout = findViewById(R.id.top_layout);
        topLayout.setVisibility(View.GONE);
        loginLinLay = findViewById(R.id.loginLinLay);
        loginLinLay.setVisibility(View.GONE);
        loginLogo = findViewById(R.id.logo);
        loginLogo.setVisibility(View.GONE);

        registerLinLay = findViewById(R.id.registerLinLay);
        registerLinLay.setVisibility(View.GONE);

        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                topLayout.setVisibility(View.VISIBLE);
                loginLogo.setVisibility(View.VISIBLE);
                loginLinLay.setVisibility(View.VISIBLE);
                registerLinLay.setVisibility(View.GONE);
            }
        });

        try {
            signIn.performClick();
        } catch (Exception e) {
            e.printStackTrace();
        }
        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                topLayout.setVisibility(View.VISIBLE);
                loginLogo.setVisibility(View.GONE);
                loginLinLay.setVisibility(View.GONE);
                registerLinLay.setVisibility(View.VISIBLE);
            }
        });


        viewPager = (ViewPager) findViewById(R.id.signInViewPager);


        currentPage = 0;
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {

/*
                if (currentPage == 0)
                    viewPager.setBackground(getResources().getDrawable(R.drawable.a1));

                if (currentPage == 1)
                    viewPager.setBackground(getResources().getDrawable(R.drawable.b1));

                if (currentPage == 2)
                    viewPager.setBackground(getResources().getDrawable(R.drawable.c1));

                if (currentPage == 3)
                    viewPager.setBackground(getResources().getDrawable(R.drawable.d1));

                if (currentPage == 4) {
                    viewPager.setBackground(getResources().getDrawable(R.drawable.d12));
                    currentPage = -1;
                }
*/
                currentPage++;
            }
        };
        /*Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                //handler.post(Update);
            }
        }, 3000, 4000);*/


        mProgressLayout = (RelativeLayout) findViewById(R.id.progress_layout);
        fb = (Button) findViewById(R.id.fb);

        final LoginButton loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions("email");

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                String accessToken = loginResult.getAccessToken().getToken();

                GraphRequest graphRequest = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Bundle facebookData = getFacebookData(object);
                        Toast.makeText(getApplicationContext(),"FB_Details"+ "" + facebookData.getString("first_name") + "" + facebookData.getString("last_name") + "" + facebookData.getString("email") + "" + facebookData.getString("gender") + "" + facebookData.getString("idFacebook"),Toast.LENGTH_LONG).show();
                        Logger.errorLog("FB_Details", "" + facebookData.getString("first_name") + "" + facebookData.getString("last_name") + "" + facebookData.getString("email") + "" + facebookData.getString("gender") + "" + facebookData.getString("idFacebook"));


                        String fName = facebookData.getString("first_name");
                        String lName = facebookData.getString("last_name");
                        String email = facebookData.getString("email");
                        String gender = facebookData.getString("gender");
                        String idFacebook = facebookData.getString("idFacebook");
                        //http:
//app.greatideasgreatlife.com/login.php?
                        // appid=89823923&
                        // fname=mike&
                        // lname=john&
                        // username=nitin.impinge@gmail.com&
                        // phone=111-111-1111&
                        // device=ios&
                        // deviceid=122121232&
                        // source=facebook
                        if(BuildConfig.DEBUG)
                            return;
                        showProgressBar();
                        doRegisterWithFacebook(idFacebook, fName, lName, email, "", "Android", Utils.getDeviceId(getContentResolver()));

                        //                        facebookData.getString("first_name");
//                        facebookData.getString("last_name");
//                        facebookData.getString("email");
//                        facebookData.getString("gender");

                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,first_name,last_name,email,gender");
                graphRequest.setParameters(parameters);
                graphRequest.executeAsync();
            }

            @Override
            public void onCancel() {
                dismissProgressBar();
            }

            @Override
            public void onError(FacebookException error) {

                dismissProgressBar();
            }
        });

        fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginButton.performClick();
            }
        });


        userEmail = (EditText) findViewById(R.id.userEmail);
        createPassword = (EditText) findViewById(R.id.createPassword);

        continueLogin = (Button) findViewById(R.id.continueLogin);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        try {
            synchronized (this) {
                this.wait(1000);
            }
        } catch (InterruptedException e) {
            /* Exception message can be captured here */
        }
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);


        Button signInButton = (Button) findViewById(R.id.sign_in_button);
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signIn();
            }
        });
        continueLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (userEmail.getText().toString().trim().equals("")) {
                    Toast.makeText(SignInActivity.this, "Please Enter Email.", Toast.LENGTH_SHORT).show();
                } else if (createPassword.getText().toString().trim().equals("")) {
                    Toast.makeText(SignInActivity.this, "Please Enter Password.", Toast.LENGTH_SHORT).show();
                } else if (!userEmail.getText().toString().trim().equals("") && !createPassword.getText().toString().trim().equals("")) {
                    boolean isValidMail = isValidMail(userEmail.getText().toString().trim());

                    if (isValidMail) {
                        String source = "normal";
                        userLogin(userEmail.getText().toString().trim(), createPassword.getText().toString(), source);
                    } else {
                        Toast.makeText(SignInActivity.this, "Please Enter valid Email.", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

    }

    private void intiView() {
        fName = (EditText) findViewById(R.id.fName);
        email = (EditText) findViewById(R.id.email);
        phone = (EditText) findViewById(R.id.phone);
        password = (EditText) findViewById(R.id.password);
        c_password = (EditText) findViewById(R.id.c_password);
        register = (Button) findViewById(R.id.continueRegister);
        mProgressLayout = (RelativeLayout) findViewById(R.id.progress_layout);
    }

    private void doRegisterWithFacebook(String idFacebook, String fName, String lName, String email, String s, String android, String deviceId) {

/*
        NetworkController.with(this).doRegisterWithFacebook(idFacebook, fName, lName, email, s, android, deviceId, new ResponseCallback<Object>(this) {
            @Override
            protected void onResponse(Object data) {
                dismissProgressBar();

                String json = new Gson().toJson(data);

                UserLogin login = new Gson().fromJson(json, UserLogin.class);
                Logger.debugLog(TAG, TAG + json);

                if (login.getStatus().getText().contains("succes")) {
                    PreferencesWrapper.getInstance(getApplicationContext()).putString(Constants.PREF_USER_ID, login.getData().getUId());
                    PreferencesWrapper.getInstance(getApplicationContext()).putString(Constants.PREF_USER_NAME, login.getData().getUsername());
                    PreferencesWrapper.getInstance(getApplicationContext()).putString(Constants.PREF_FIRST_NAME, login.getData().getFname());
                    PreferencesWrapper.getInstance(getApplicationContext()).putString(Constants.PREF_LAST_NAME, login.getData().getLname());
                    PreferencesWrapper.getInstance(getApplicationContext()).putString(Constants.PREF_USER_PHONE, login.getData().getPhone());
                    PreferencesWrapper.getInstance(getApplicationContext()).putBoolean(Constants.PREF_USER_LOGGED_IN, true);
                    Intent loginIntent = new Intent(SignInActivity.this, MainActivity.class);
                    if (login.getData().getPhone().equals("") || login.getStatus().getCode() == 2) {
                        loginIntent.putExtra("showPhoneDialog", true);
                    }
                    startActivity(loginIntent);
                    finish();
                } else {
                    Toast.makeText(SignInActivity.this, login.getStatus().getText(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            protected void onFailure(Throwable t) {
                dismissProgressBar();
                Toast.makeText(SignInActivity.this, "Some error occurred", Toast.LENGTH_SHORT).show();
            }
        });
*/
    }

    public void launchWebView(View v) {
        Intent intent = new Intent(SignInActivity.this, SignInActivity.class/*WebViewActivity.class*/);
        intent.putExtra("url", Constants.FORGOT_URL);
        intent.putExtra("showLoader",true);
        startActivity(intent);

    }

    private void dismissProgressBar() {
        try {
            mProgressLayout.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showProgressBar() {
        mProgressLayout.setVisibility(View.VISIBLE);
    }

    private void userLogin(String email, String password, String source) {
        showProgressBar();
        /*NetworkController.with(this).userLogin(email, password, source, new ResponseCallback<Object>(this) {

            @Override
            protected void onResponse(Object data) {
                dismissProgressBar();
                String json = new Gson().toJson(data);

                try {
                    UserLogin login = new Gson().fromJson(json, UserLogin.class);
                    Logger.debugLog(TAG, TAG + json);

                    UserLoginError loginError = null;
                    if (login == null) {
                        loginError = new Gson().fromJson(json, UserLoginError.class);
                        if (loginError.getStatus().getError().contains("yes")) {
                            Toast.makeText(SignInActivity.this, login.getStatus().getText(), Toast.LENGTH_SHORT).show();
                            return;
                        }
                    }
                    if (login.getStatus().getError().contains("no")) {
                        PreferencesWrapper.getInstance(getApplicationContext()).putString(Constants.PREF_USER_ID, login.getData().getUId());
                        PreferencesWrapper.getInstance(getApplicationContext()).putString(Constants.PREF_USER_NAME, login.getData().getUsername());
                        PreferencesWrapper.getInstance(getApplicationContext()).putString(Constants.PREF_FIRST_NAME, login.getData().getFname());
                        PreferencesWrapper.getInstance(getApplicationContext()).putString(Constants.PREF_LAST_NAME, login.getData().getLname());
                        PreferencesWrapper.getInstance(getApplicationContext()).putString(Constants.PREF_USER_PHONE, login.getData().getPhone());
                        PreferencesWrapper.getInstance(getApplicationContext()).putBoolean(Constants.PREF_USER_LOGGED_IN, true);
                        Intent loginIntent = new Intent(SignInActivity.this, MainActivity.class);
                        if (login.getData().getPhone().equals("") || login.getStatus().getCode() == 2) {
                            loginIntent.putExtra("showPhoneDialog", true);
                        }
                        startActivity(loginIntent);
                        finish();
                    } else {
                        Toast.makeText(SignInActivity.this, login.getStatus().getText(), Toast.LENGTH_SHORT).show();
                    }

                    if (json.contains("not found")) {
                        Toast.makeText(SignInActivity.this, login.getStatus().getText(), Toast.LENGTH_SHORT).show();
                        return;
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(SignInActivity.this, "Server issue", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            protected void onFailure(Throwable t) {
                dismissProgressBar();
                Toast.makeText(SignInActivity.this, "Some error occurred", Toast.LENGTH_SHORT).show();
            }
        });
        */
    }

    @Override
    protected void onStart() {
        super.onStart();
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        updateUI(account);
    }

    private void updateUI(GoogleSignInAccount account) {
    }

    //Gmail SignIn_____
    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    public void clickSignup(View cv) {
        signUp.performClick();
    }

    public void clickSignin(View cv) {
        signIn.performClick();
    }

    //Gmail UserDetails________
    private void handleSignInResult(Task<GoogleSignInAccount> task) {
        try {
            GoogleSignInAccount account = task.getResult(ApiException.class);
            updateUI(account);

            String displayNAme = account.getDisplayName();
            String email = account.getEmail();
            String idToken = account.getIdToken();
            Uri photoUrl = account.getPhotoUrl();
            String id = account.getId();

            Toast.makeText(getApplicationContext(), displayNAme + "" + email + "" + idToken + "" + photoUrl + "" + id, Toast.LENGTH_LONG).show();
            Logger.debugLog("TAG_1", "" + displayNAme + "" + email + "" + idToken + "" + photoUrl + "" + id);
            if(BuildConfig.DEBUG)
                return;
            //http://app.greatideasgreatlife.com/login.php?appid=89823923&fname=mike&lname=john&username=nitin.impinge@gmail.com&phone=111-111-
            //1111&device=ios&deviceid=122121232&source=google
            doRegisterWithGoogle(id, displayNAme, "", email, "", "Android", Utils.getDeviceId(getContentResolver()));

        } catch (ApiException e) {
            Logger.verboseLog("TAG", "signInResult:failed code=" + e.getStatusCode());
            updateUI(null);
        }
    }


    private void doRegisterWithGoogle(String id, String displayNAme, String s, String email, String s1, String android, String deviceId) {

        showProgressBar();
        /*NetworkController.with(this).doRegisterWithGoogle(id, displayNAme, s, email, s1, android, deviceId, new ResponseCallback<Object>(this) {
            @Override
            protected void onResponse(Object data) {

                dismissProgressBar();

                String json = new Gson().toJson(data);

                UserLogin login = new Gson().fromJson(json, UserLogin.class);
                if(login==null)
                    return;
                if (login.getStatus().getText().contains("succes")) {
                    //Toast.makeText(SignInActivity.this, login.getStatus().getText(), Toast.LENGTH_SHORT).show();
                    PreferencesWrapper.getInstance(getApplicationContext()).putString(Constants.PREF_USER_ID, login.getData().getUId());
                    PreferencesWrapper.getInstance(getApplicationContext()).putString(Constants.PREF_USER_NAME, login.getData().getUsername());
                    PreferencesWrapper.getInstance(getApplicationContext()).putString(Constants.PREF_FIRST_NAME, login.getData().getFname());
                    PreferencesWrapper.getInstance(getApplicationContext()).putString(Constants.PREF_LAST_NAME, login.getData().getLname());
                    PreferencesWrapper.getInstance(getApplicationContext()).putString(Constants.PREF_USER_PHONE, login.getData().getPhone());
                    PreferencesWrapper.getInstance(getApplicationContext()).putBoolean(Constants.PREF_USER_LOGGED_IN, true);
                    Intent loginIntent = new Intent(SignInActivity.this, MainActivity.class);
                    if (login.getData().getPhone().equals("") || login.getStatus().getCode() == 2) {
                        loginIntent.putExtra("showPhoneDialog", true);
                    }
                    startActivity(loginIntent);
                    finish();
                } else {
                    Toast.makeText(SignInActivity.this, login.getStatus().getText(), Toast.LENGTH_SHORT).show();
                }

                //GetProductUsersWrapper response = new Gson().fromJson(json, GetProductUsersWrapper.class);

            }

            @Override
            protected void onFailure(Throwable t) {
                dismissProgressBar();
            }

        });*/
    }

    public final static boolean isValidMail(CharSequence target) {
        if (TextUtils.isEmpty(target)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

//    private boolean isValidMail(String email) {
//        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
//    }

    private Bundle getFacebookData(JSONObject object) {
        Bundle bundle = new Bundle();

        try {
            String id = object.getString("id");
            URL profile_pic;
            try {
                profile_pic = new URL("https://graph.facebook.com/" + id + "/picture?type=large");
                Log.i("profile_pic", profile_pic + "");
                bundle.putString("profile_pic", profile_pic.toString());
            } catch (MalformedURLException e) {
                e.printStackTrace();
                return null;
            }

            bundle.putString("idFacebook", id);
            if (object.has("first_name"))
                bundle.putString("first_name", object.getString("first_name"));
            if (object.has("last_name"))
                bundle.putString("last_name", object.getString("last_name"));
            if (object.has("email"))
                bundle.putString("email", object.getString("email"));
            if (object.has("gender"))
                bundle.putString("gender", object.getString("gender"));


//            prefUtil.saveFacebookUserInfo(object.getString("first_name"),
//                    object.getString("last_name"),object.getString("email"),
//                    object.getString("gender"), profile_pic.toString());

        } catch (Exception e) {
            Log.d(TAG, "BUNDLE Exception : " + e.toString());
        }

        return bundle;
    }

    //fb LOGOUT
    private void deleteAccessToken() {
        AccessTokenTracker accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(
                    AccessToken oldAccessToken,
                    AccessToken currentAccessToken) {

                if (currentAccessToken == null) {
                    //User logged out
                    //prefUtil.clearToken();
//                    public void clearToken() {
//                        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(activity);
//                        SharedPreferences.Editor editor = prefs.edit();
//                        editor.clear();
//                        editor.apply(); // This line is IMPORTANT !!!
//                    }
                    LoginManager.getInstance().logOut();
                }
            }
        };
    }

    @Override
    public void onClick(View v) {
        if (email.getText().toString().trim().equals("")) {
            Toast.makeText(this, "Please Enter Email", Toast.LENGTH_SHORT).show();
        } else if (!email.getText().toString().trim().equals("")) {
            boolean emailValid = isValidMail(email.getText().toString().trim());
            if (emailValid) {
                if (password.getText().toString().trim().equals("")) {
                    Toast.makeText(this, "Please Enter Password", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (c_password.getText().toString().trim().equals("")) {
                    Toast.makeText(this, "Please Re-enter Password", Toast.LENGTH_SHORT).show();
                    return;
                } else if (!password.getText().toString().equals(c_password.getText().toString())) {
                    Toast.makeText(this, "Passwords doesn't match.", Toast.LENGTH_SHORT).show();
                    return;
                } else if (fName.getText().toString().trim().equals("")) {
                    Toast.makeText(this, "Please Enter Name", Toast.LENGTH_SHORT).show();
                } else {
                    if (!phone.getText().toString().trim().equals("")) {
                        boolean validMobile = isValidMobile(phone.getText().toString());
                        if (validMobile) {
                            showProgressBar();
                            doRegister(fName.getText().toString().trim(), ".", email.getText().toString().trim(), phone.getText().toString().trim(), password.getText().toString().trim(), "Android", deviceId);
                        } else {
                            Toast.makeText(this, "Please Enter a valid Number", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        showProgressBar();
                        doRegister(fName.getText().toString().trim(), ".", email.getText().toString().trim(), phone.getText().toString().trim(), password.getText().toString().trim(), "Android", deviceId);
                    }
                }
            } else {
                Toast.makeText(this, "Please Enter valid Email", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void doRegister(String fName, String lName, String email, String phone, String password, String deviceOs, String deviceID) {

        /*NetworkController.with(this).doRegister(fName, lName, email, phone, password, deviceOs, deviceID, new ResponseCallback<Object>(this) {
            @Override
            protected void onResponse(Object data) {

                dismissProgressBar();

                try {
                    String json = new Gson().toJson(data);
                    StatusPojo userRegister = new Gson().fromJson(json, StatusPojo.class);
                    Logger.debugLog(TAG, TAG + json);


                    if (userRegister.getCode().equals(0)) {
                        Toast.makeText(SignInActivity.this, userRegister.getText(), Toast.LENGTH_SHORT).show();
                        Intent loginIntent = new Intent(SignInActivity.this, SignInActivity.class);
                        startActivity(loginIntent);
                        finish();
                    } else {
                        Toast.makeText(SignInActivity.this, userRegister.getText().toString(), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                //GetProductUsersWrapper response = new Gson().fromJson(json, GetProductUsersWrapper.class);
            }

            @Override
            protected void onFailure(Throwable t) {
                dismissProgressBar();
                Toast.makeText(SignInActivity.this, "Some error occurred", Toast.LENGTH_SHORT).show();
            }


        });*/
    }

    private boolean isValidMobile(String phone) {
        boolean check = false;
        if (!Pattern.matches("[a-zA-Z]+", phone)) {
            if (phone.length() < 10) {
                // if(phone.length() != 10) {
                check = false;
                //txtPhone.setError("Not Valid Number");
            } else {
                check = true;
            }
        } else {
            check = false;
        }
        return check;
    }


    public static void colorizeToolbar(Toolbar toolbarView, int toolbarIconsColor, Activity activity) {
        try {
            final PorterDuffColorFilter colorFilter
                    = new PorterDuffColorFilter(toolbarIconsColor, PorterDuff.Mode.MULTIPLY);

            for (int i = 0; i < toolbarView.getChildCount(); i++) {
                final View v = toolbarView.getChildAt(i);

                try {
                    //Step 1 : Changing the color of back button (or open drawer button).
                    if (v instanceof ImageButton) {
                        //Action Bar back button
                        ((ImageButton) v).getDrawable().setColorFilter(colorFilter);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    if (v instanceof ActionMenuView) {
                        for (int j = 0; j < ((ActionMenuView) v).getChildCount(); j++) {

                            //Step 2: Changing the color of any ActionMenuViews - icons that
                            //are not back button, nor text, nor overflow menu icon.
                            final View innerView = ((ActionMenuView) v).getChildAt(j);

                            if (innerView instanceof ActionMenuItemView) {
                                int drawablesCount = ((ActionMenuItemView) innerView).getCompoundDrawables().length;
                                for (int k = 0; k < drawablesCount; k++) {
                                    if (((ActionMenuItemView) innerView).getCompoundDrawables()[k] != null) {
                                        final int finalK = k;

                                        //Important to set the color filter in seperate thread,
                                        //by adding it to the message queue
                                        //Won't work otherwise.
                                        innerView.post(new Runnable() {
                                            @Override
                                            public void run() {
                                                ((ActionMenuItemView) innerView).getCompoundDrawables()[finalK].setColorFilter(colorFilter);
                                            }
                                        });
                                    }
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    //Step 3: Changing the color of title and subtitle.
                    toolbarView.setTitleTextColor(toolbarIconsColor);
                    toolbarView.setSubtitleTextColor(toolbarIconsColor);
                } catch (Exception e) {
                    e.printStackTrace();
                }
    /*        //Step 4: Changing the color of the Overflow Menu icon.
            setOverflowButtonColor(activity, colorFilter);*/
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (!this.isFinishing())
            finish();
    }

    @Override
    protected void onStop() {
        super.onStop();
        doLogOut();

    }
    private void doLogOut() {
        try {
            AccessToken token;
            token = AccessToken.getCurrentAccessToken();
            if (token != null) {
                LoginManager.getInstance().logOut();
            }

            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestEmail()
                    .build();
            GoogleSignInClient mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
            mGoogleSignInClient.signOut().addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
//                    launchSignInActivity(null);
                }
            });

            PreferencesWrapper.getInstance(getApplicationContext()).clearAllPreferences();

        } catch (Exception e) {
            e.printStackTrace();
//            launchSignInActivity(null);
            PreferencesWrapper.getInstance(getApplicationContext()).clearAllPreferences();
        }
    }
}
