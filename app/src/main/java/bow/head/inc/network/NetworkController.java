package bow.head.inc.network;

import android.content.Context;

public class NetworkController {
    private static final String TAG = NetworkController.class.getSimpleName();
    private Context mContext;

    public static NetworkController with(Context context) {
        return new NetworkController(context);
    }

    private NetworkController(Context mContext) {
        this.mContext = mContext;
    }


    /*public void getImageList(String key, String q, String image_type, String page, String per_page, String safesearch, String min_width, String min_height, String orientation, String lang, ResponseCallback<Object> callback) {
        RestClient.get().getImageList(key,q,image_type,page,per_page,safesearch,min_width,min_height,orientation,lang).enqueue(callback);
    }*/

    public  void userLogin(String email,String password,String source,ResponseCallback<Object> callback){
        RestClient.get().userLogin(email,password,source).enqueue(callback);
    }
}
