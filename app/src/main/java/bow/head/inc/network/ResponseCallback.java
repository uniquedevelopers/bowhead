package bow.head.inc.network;

import android.content.Context;

import bow.head.inc.Utils.Utils;

import com.example.hitesh.bowheadinc.R;
import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public abstract class ResponseCallback<T> implements Callback<T> {

    private static final String TAG = "RetrofitCallBack";
    private Context mContext;

    public ResponseCallback(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public void onResponse(Call<T> call, Response<T> data) {
        String json=null;
        try {
            json = new Gson().toJson(data.body());
        }catch (OutOfMemoryError error){
            Utils.getInstance(null).showToast("mContext.getString(R.string.error_out_of_memory)", false);
        }


        if(data.code()==504 || data.code()==404 || json==null){
            onFailure(new Throwable());
            Utils.getInstance(null).showToast("mContext.getString(R.string.error_something_went_wrong)", true);
        } else
            onResponse(data.body());
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        Utils.handleRetrofitExceptions(t);
        onFailure(t);
    }

    protected abstract void onResponse(T data);

    protected abstract void onFailure(Throwable t);
}
